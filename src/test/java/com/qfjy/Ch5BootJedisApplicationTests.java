package com.qfjy;

import com.qfjy.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Ch5BootJedisApplicationTests {

    @Autowired
    private UserServiceImpl userService;

    /**
     * JAVA操作Redis (Jedis客户端技术）
     */
    @Test
    void contextLoads() {

        //userService.testJedis();
        for(int i=1;i<10;i++) {
            String result = userService.selectGetName("java2201");
            System.out.println(result);

        }
    }

}
