package com.qfjy.config;

import com.qfjy.interceptor.FwInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class MyWebAppConfigurer implements WebMvcConfigurer {
    @Resource
    private FwInterceptor fwInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //如果interceptor中不注入redis或其他项目可以直接new
        //4、将拦截器对像手动添加到拦截器链中（在addInterceptors方法中添加）
        registry.addInterceptor(fwInterceptor).addPathPatterns("/**").excludePathPatterns( "/login" )
                .excludePathPatterns( "/static/**" );
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler( "/static/**" ).
                addResourceLocations( "classpath:/static/" );
    }




}
