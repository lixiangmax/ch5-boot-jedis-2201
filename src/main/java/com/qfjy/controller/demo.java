package com.qfjy.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/demo")
public class demo {

    @Autowired
    private JedisPool jedisPool;

    @RequestMapping("/info")
    public Object test(){
        Jedis jedis = jedisPool.getResource();
        Map<String,String> result=new HashMap<>();
        result.put("访问成功","2s内访问次数为"+jedis.get("count"));
        jedis.close();
        return  result;
    }


}
