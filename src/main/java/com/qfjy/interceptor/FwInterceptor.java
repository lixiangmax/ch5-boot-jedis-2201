package com.qfjy.interceptor;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.naming.LimitExceededException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Component
@Slf4j
public class FwInterceptor implements HandlerInterceptor {

    @Autowired
    private JedisPool jedisPool;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Jedis jedis=jedisPool.getResource();
        String ip = request.getRemoteAddr();
        //每次访问+1
        Long  count = jedis.incr(ip);
        //次数时间2秒
        jedis.pexpire(ip,2000);
        log.info(ip);
        log.info(jedis.get(ip));
        if(count>2){
            //移除过期时间 锁定count值
            jedis.persist(ip);
            try {
                //60s后恢复
                Thread.sleep(6000);
                jedis.set(ip,String.valueOf(0));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        jedis.close();
        return true;
    }



}
