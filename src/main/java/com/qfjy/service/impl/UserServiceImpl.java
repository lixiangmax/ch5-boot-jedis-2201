package com.qfjy.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @ClassName UserServiceImpl
 * @Description TODO
 * @Author guoweixin
 * @Date 2022/7/5
 * @Version 1.0
 */
@Service
@Slf4j
public class UserServiceImpl {

    /**JedisPool连接池*/
    @Autowired
    private JedisPool jedisPool;

    /**
     * #TODO 测试 JAVA操作Redis是否整合
     */
    public void testJedis(){
        //1 获取连接
        Jedis jedis=jedisPool.getResource();
        //2 通过jedis连接来操作Redis数 据库
         //Redis有哪些命令，Jedis就有哪些方法
         jedis.set("name","java2201班");

        String result= jedis.get("name");
        log.info("查询Redis数据是：{}",result);
        //3 关闭连接Jedis
         jedis.close();

    }

    /**
     * # TODO java操作Redis String案例
     *   Redis就是解决MYSQL对高并发场景不足解决方案。（缓存）
     *   1、判断Redis中是否存在该数据，
     *   2、如果数据不存在，查询的是MYSQL，MYSQL中得到结果，并存入Redis
     *   3、如果数据在Redis中存在，查询Redis直接返回
     */
     public String selectGetName(String key){
         //1 获取连接
         Jedis jedis=jedisPool.getResource();
         String result=null;

         if(!jedis.exists(key)){
              result="南京JAVA";
             log.info("查询的是MYSQL---》"+result);

             jedis.set(key,result);

         }else{
             log.info("查询的是Redis--》");
             result= jedis.get(key);

         }

         //2、关闭连接
         jedis.close();


         return result;
     }

}
