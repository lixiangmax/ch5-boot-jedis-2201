package com.qfjy.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @ClassName JedisConfig
 * @Description TODO
 * @Author guoweixin
 * @Date 2022/7/5
 * @Version 1.0
 */
@Configuration
public class JedisConfig {

    //最大连接数
    @Value("${spring.redis.jedis.pool.max-active}")
    private int maxTotal;

    //最大空闲数
    @Value("${spring.redis.jedis.pool.max-idle}")
    private int maxIdle;
    //最小空闲数
    @Value("${spring.redis.jedis.pool.min-idle}")
    private int minIdle;

    //服务器IP地址
    @Value("${spring.redis.host}")
    private String host;
    //端口号
    @Value("${spring.redis.port}")
    private int port;
    //超时时间
    @Value("${spring.redis.timeout}")
    private int timeout;
    //密码
    @Value("${spring.redis.password}")
    private String password;
    //数据库下标
    @Value("${spring.redis.database}")
    private int database;
    /**
     * Java操作MYSQL（技术JDBC)
     * 1、创建数据库连接池DruidDataSource
     * 2、Connection连接
     * 3、执行连接业务逻辑
     * 4、关闭connection
     *
     * SpringBoot java操作Redis (技术Jedis)
     * 1、连接池相关配置 JedisPoolConfig    JedisPool(连接）
     * 2、连接 Jedis
     * 3、执行业务逻辑
     * 4、关闭连接
     *
     */
    @Bean
    public JedisPool jedisPool(){

        /**
         * 1、连接池相关配置
         */
        JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
        //最大连接数
        jedisPoolConfig.setMaxTotal(maxTotal);
        //最大空闲数
        jedisPoolConfig.setMaxIdle(maxIdle);
        //最小空闲数
        jedisPoolConfig.setMinIdle(minIdle);
        /**
         * 2、连接池构建
         *   public JedisPool(GenericObjectPoolConfig<Jedis> poolConfig, String host, int port, int timeout, String password, int database)
         */
        return new JedisPool(jedisPoolConfig,host,port,timeout,password,database);
    }
}
